#ifndef FILE_SERIALIZER_HPP
#define FILE_SERIALIZER_HPP

#include <string>

#include "BufferedEntity.hpp"


class FileSerializer : public BufferedEntity {
public:
	FileSerializer(std::string path) : path(path) {}

	virtual void serialize() = 0;

protected:
	std::string path;
};

#endif
