#include <cstdlib>

#include "BinLedCommand.hpp"


std::vector<uint8_t> BinLedCommand::toBuffer(std::vector<uint8_t> arg, uint32_t randNum, char *timeStr) {
	std::vector<uint8_t> buffer;
	int len = arg.size() + 30;

	buffer.reserve(len);

	buffer.insert(buffer.end(), header.begin(), header.end());

	buffer.push_back(CMD_BIN_HEADER);
	buffer.push_back((len >> 8) & 0xff);
	buffer.push_back(len & 0xff);
	buffer.push_back(nonce);

	buffer.insert(buffer.end(), string.begin(), string.end());

	for (int i = 0; i < sizeof(randNum); i++)
		buffer.push_back((randNum >> i) & 0xff);

	buffer.insert(buffer.end(), arg.begin(), arg.end());

	free(timeStr);

	return buffer;
}
