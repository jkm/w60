#ifndef LED_SENDER_HPP
#define LED_SENDER_HPP

#include "Socket.hpp"


class LedSender {
public:
	LedSender() : sock(nullptr) {}

	~LedSender() {
		delete sock;
	}

	void send(std::vector<uint8_t> buffer);

protected:
	Socket *sock;
};

#endif
