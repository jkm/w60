#ifndef HDS_SERIALIZER_HPP
#define HDS_SERIALIZER_HPP

#include "FileSerializer.hpp"


class HdsSerializer : public FileSerializer {
public:
	HdsSerializer(std::string path) : FileSerializer(path) {}

	void serialize();
};

#endif
