#ifndef BYTE_ARRAY_H
#define BYTE_ARRAY_H

#include <array>
#include <cstddef>
#include <vector>

#include <stdint.h>


template<std::size_t N>
class ByteArray {
	private:
		std::array<uint8_t, N> buffer;
		int position;

	public:
		ByteArray();

		std::vector<uint8_t> toVector();

		// b
		std::array<uint8_t, N> getBuffer();

		// e
		std::size_t getLength();

		// f
		int getPosition();

		// h
		void setTwoBytes(int value);

		// i
		void setTwoPositionBytes(int position, int value);

		// j
		void setThreeBytes(int value);

		// k
		void setThreePositionBytes(int position, int value);

		// l
		void setFourBytes(int value);

		// m
		void setFourPositionBytes(int position, int value);

		// n
		void setOneByte(int value);

		// o
		void setOnePositionByte(int position, int value);

		// p
		void incrementPosition(int step);

		void print();
};

#endif
