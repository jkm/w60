#ifndef SOCKET_HPP
#define SOCKET_HPP

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <string>
#include <vector>

#include "conf.hpp"


class Socket {
public:
	Socket();
	virtual ~Socket();

	bool create();
	bool bind(const int port);
	bool listen() const;
	bool accept(Socket &) const;
	bool connect(const std::string host, const int port);
	bool send(const std::vector<uint8_t> data) const;
	int recv(std::string &) const;

	bool isValid() const;

private:
	int sock;
	sockaddr_in addr;
};

#endif
