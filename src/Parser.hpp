#ifndef PARSER_HPP
#define PARSER_HPP

#include <argparse/argparse.hpp>

#include "UserCommand.hpp"


class AbstractHandler {
public:
	virtual void handle() = 0;
};

class ParserHandler : public AbstractHandler, public argparse::ArgumentParser {
public:
	ParserHandler(std::string name) : argparse::ArgumentParser(name) {}
};

class SetHdsParserHandler : public ParserHandler {
public:
	SetHdsParserHandler() : ParserHandler("set-hds") {
		add_description("Set HDS file to display");
		add_argument("path").help("Path to HDS file");
	}

	void handle();
};

class SetTextParserHandler : public ParserHandler {
public:
	SetTextParserHandler() : ParserHandler("set-text") {
		add_description("Set text to display");
		add_argument("path").help("Path to JSON file with screen metadata");
		add_argument("text").help("Text to display");
	}

	void handle();
};

class SetLuminanceParserHandler : public ParserHandler {
public:
	SetLuminanceParserHandler() : ParserHandler("set-luminance") {
		add_description("Set luminance of LED array");
		add_argument("luminance").help("Luminance level to be set").scan<'i', int>();
	}

	void handle();
};

class RebootParserHandler : public ParserHandler {
public:
	RebootParserHandler() : ParserHandler("reboot") {
		add_description("Reboot the LED array");
	}

	void handle();
};

class ProgramParserHandler : public ParserHandler {
public:
	ProgramParserHandler() : ParserHandler("w60"), parsers({
		new RebootParserHandler(),
		new SetLuminanceParserHandler(),
		new SetHdsParserHandler(),
		new SetTextParserHandler(),
	}) {
		add_description("Simple program to control W60-based LED array");

		for (auto & p : parsers)
			add_subparser(*p);
	}

	~ProgramParserHandler() {
		for (auto & p : parsers)
			delete p;
	}

	void handle();

private:
	std::vector<ParserHandler *> parsers;
};

#endif
