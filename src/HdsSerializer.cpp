#include <algorithm>
#include <fstream>
#include <stdexcept>

#include "LedCommand.hpp"
#include "HdsSerializer.hpp"


void HdsSerializer::serialize() {
	std::ifstream is(path, std::ifstream::binary);
	if (!is)
		throw std::invalid_argument("Can't open the file");

	std::vector<uint8_t> hdsBuf(std::istreambuf_iterator<char>(is), {});

	auto it = std::search(hdsBuf.begin(), hdsBuf.end(), zipHeader.begin(), zipHeader.end());
	if (it != hdsBuf.end())
		hdsBuf.resize(it - hdsBuf.begin());

	insertBuffer(hdsBuf);
}
