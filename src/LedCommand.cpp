#include "LedCommand.hpp"


code_t LedCommand::getCode() {
	return code;
}

uint8_t LedCommand::getLength() {
	return length;
}

uint8_t LedCommand::getNonce() {
	return nonce;
}
