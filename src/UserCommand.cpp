#include "BufferedEntity.hpp"
#include "UserCommand.hpp"


void UserCommand::setSerializer(LedCommandSerializer *s) {
	delete serializer;
	serializer = s;
}

void BinaryUserCommand::handle(std::vector<BufferedEntity *> buffers) {
	uint32_t n = rand() % 0xffffffff;
	uint8_t p = (n >> 8) & 0xff;
	uint8_t q = n & 0xff;

	std::vector<uint8_t> arg;

	arg = { 0x05, p, q, 0x00, 0x02 };
	setSerializer(new LedCommandSerializer(new FileStartLedCommand()));
	send(serializer->serialize(arg));
	usleep(DISPLAY_COMMAND_SLEEP_US);

	arg = { 0x05, p, q, 0x00, 0x00 };
	setSerializer(new LedCommandSerializer(new FileDataLedCommand()));
	send(serializer->serialize(arg, buffers));
	usleep(DISPLAY_COMMAND_SLEEP_US);

	arg = { 0x03, p, q };
	setSerializer(new LedCommandSerializer(new FileEndLedCommand()));
	send(serializer->serialize(arg));
	usleep(DISPLAY_COMMAND_SLEEP_US);
}

void RebootUserCommand::handle() {
	std::vector<uint8_t> arg = { 0x02, 0x02 };
	send(serializer->serialize(arg));
	usleep(DISPLAY_COMMAND_SLEEP_US);
}

void SetLuminanceUserCommand::handle(uint8_t luminance) {
	std::vector<uint8_t> arg = { 0x0f, 0x00, 0x00, luminance, 0x31, 0x31, 0x00, 0xd7, 0xdc, 0x00, 0xd7, 0xdc, 0x00, 0xd7, 0xdc };
	send(serializer->serialize(arg));
	usleep(DISPLAY_COMMAND_SLEEP_US);
}

void SetHdsUserCommand::handle(std::string path) {
	HdsSerializer *h = new HdsSerializer(path);
	h->serialize();

	BinaryUserCommand::handle({ h });

	delete h;
}

void SetTextUserCommand::handle(std::string path, std::string text) {
	JsonSerializer *s = new JsonSerializer(path);
	s->serialize();

	BitmapRenderer *r = new BitmapRenderer(text);
	r->render();

	BinaryUserCommand::handle({ s, r });

	delete s;
	delete r;
}
