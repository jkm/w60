#include "LedSender.hpp"

#include "conf.hpp"


void LedSender::send(std::vector<uint8_t> buffer) {
	if (sock == nullptr) {
		sock = new Socket();
		sock->create();
		sock->connect(DISPLAY_IP, DISPLAY_PORT);
	}

	sock->send(buffer);
}
