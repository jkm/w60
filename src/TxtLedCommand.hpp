#ifndef TXT_LED_COMMAND_HPP
#define TXT_LED_COMMAND_HPP

#include "LedCommand.hpp"


class TxtLedCommand : public LedCommand {
public:
	TxtLedCommand(code_t code, uint8_t length, uint8_t nonce) : LedCommand(code, length, nonce) {};

	std::vector<uint8_t> toBuffer(std::vector<uint8_t> arg, uint32_t randNum, char *timeStr);
};

#endif
