#include <algorithm>
#include <fstream>
#include <iostream>

#include "ByteArray.hpp"
#include "JsonSerializer.hpp"


void JsonSerializer::parseJson() {
	std::ifstream file(path);
	file >> root;
}

void JsonSerializer::serialize() {
	parseJson();

	Json::Value program = root["screen"]["programs"][0];
	Json::Value area = program["areas"][0];
	Json::Value widget = area["widgets"][0]["content"];
	Json::Value effect = widget["effect"];

	insertBuffer(serializeScreen(program));
	insertBuffer(serializeProgram(program));
	insertBuffer(serializeArea(area));
	insertBuffer(serializeEffect(effect));
}

std::vector<uint8_t> JsonSerializer::serializeProgram(Json::Value value) {
	ByteArray<256> array;

	array.setOneByte(0x48);
	array.setOneByte(0x50);
	array.setTwoBytes(0x00);
	array.setOneByte(0x01);						// arrayList.size() = 1
	array.setOneByte(0x00);						// playByTime = false
	array.setThreeBytes(std::stoi(value["startTime"].asString()));
	array.setThreeBytes(std::stoi(value["endTime"].asString()));
	array.setOneByte(0x00);						// playByDuration = false
	array.setThreeBytes(value["duration"].asInt());
	array.setOneByte(0x7f);						// play{Monday..Sunday} = true
	array.setOneByte(0x00);						// playByDate = false
	array.setFourBytes(std::stoi(value["startDate"].asString()));
	array.setFourBytes(std::stoi(value["endDate"].asString()));
	array.setFourBytes(0x00);
	array.setFourBytes(0x00);

	std::string uuid = value["uuid"].asString();
	uuid.erase(std::remove(uuid.begin(), uuid.end(), '-'), uuid.end());

	for (int i = 0; i < 16; i++)
		array.setOneByte(std::stoi(uuid.substr(i * 2, 2), nullptr, 16));

	array.setOneByte(0xff);						// HORIZONTAL_MASK
	array.setFourBytes(0x00);
	array.setFourBytes(0x00);
	array.setTwoPositionBytes(2, array.getPosition());
	array.setFourBytes(0x00);
	array.setOneByte(0x44);
	array.setTwoBytes(0xffff);
	array.setTwoBytes(0xffff);

	return array.toVector();
}

std::vector<uint8_t> JsonSerializer::serializeScreen(Json::Value value) {
	ByteArray<256> array;

	array.setOneByte(0x48);
	array.setOneByte(0x43);
	array.setOneByte(0x00);
	array.setTwoBytes(0x01);					// arrayList.size()
	array.setTwoBytes(0xffff);
	array.setTwoBytes(0xffff);
	array.setTwoBytes(0x03);
	array.setFourBytes(0x00);
	array.setFourBytes(0x00);
	array.setFourBytes(0x00);
	array.setFourBytes(0x00);
	array.setOnePositionByte(2, array.getPosition());
	array.setFourBytes(0x1f);

	return array.toVector();
}

std::vector<uint8_t> JsonSerializer::serializeArea(Json::Value value) {
	ByteArray<256> array;

	array.setOneByte(0x48);
	array.setOneByte(0x41);
	array.setTwoBytes(0x00);
	array.setTwoBytes(value["x"].asInt());
	array.setTwoBytes(value["y"].asInt());
	array.setTwoBytes(value["w"].asInt());
	array.setTwoBytes(value["h"].asInt());
	array.setTwoBytes(0x01);					// childCount
	array.setOneByte(0x00);
	array.setTwoBytes(0x00);
	array.setFourBytes(0x00);
	array.setFourBytes(0x00);
	array.setFourBytes(0x00);
	array.setFourBytes(0x00);
	array.setFourBytes(0x00);
	array.setTwoPositionBytes(2, array.getPosition());
	array.setFourBytes(0x29);

	return array.toVector();
}

std::vector<uint8_t> JsonSerializer::serializeEffect(Json::Value value) {
	ByteArray<256> array;

	array.setTwoBytes(0x00);
	array.setOneByte(0x30);
	array.setTwoBytes(0x01);					// i2
	array.setTwoBytes(value["playCount"].asInt());
	array.setTwoBytes(0x01);
	array.setTwoBytes(0xffff);
	array.setTwoBytes(0x00);
	array.setTwoBytes(0xc0);
	array.setFourBytes(0x00);
	array.setOneByte(0x00);
	array.setTwoBytes(0x55);
	array.setTwoPositionBytes(0, array.getPosition());
	array.setOneByte(value["effects"].asInt());
	array.setOneByte(0xc9);						// clearImmediately
	array.setTwoBytes(value["speed"].asInt());
	array.setOneByte(value["holdTime"].asInt());
	array.setTwoBytes(0x00);
	array.setFourPositionBytes(15, array.getPosition());

	return array.toVector();
}
