#ifndef BITMAP_RENDERER_HPP
#define BITMAP_RENDERER_HPP

#include <string>

#include "BufferedEntity.hpp"


class BitmapRenderer : public BufferedEntity {
public:
	BitmapRenderer(std::string text);

	void render();

private:
	std::string text;
};

#endif
