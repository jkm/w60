#ifndef JSON_SERIALIZER_HPP
#define JSON_SERIALIZER_HPP

#include <json/json.h>

#include "FileSerializer.hpp"


class JsonSerializer : public FileSerializer {
public:
	JsonSerializer(std::string path) : FileSerializer(path) {}

	void parseJson();
	void serialize();

private:
	Json::Value root;

	std::vector<uint8_t> serializeProgram(Json::Value value);
	std::vector<uint8_t> serializeScreen(Json::Value value);
	std::vector<uint8_t> serializeArea(Json::Value value);
	std::vector<uint8_t> serializeEffect(Json::Value value);
};

#endif
