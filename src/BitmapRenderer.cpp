#include <iostream>

#include "conf.hpp"
#include "font.hpp"
#include "BitmapRenderer.hpp"


BitmapRenderer::BitmapRenderer(std::string text) {
	this->text = text;
}

void BitmapRenderer::render() {
	buffer.reserve(DISPLAY_WIDTH * DISPLAY_HEIGHT / 8);

	int length = text.size();
	int char_num = DISPLAY_WIDTH / 8;
	if (length > char_num)
		return;

	bool isEven = length % 2 == 0;
	int offset = (char_num - length) / 2;
	char c1, c2;
	std::array<uint8_t, DISPLAY_HEIGHT> c_arr1, c_arr2;

	for (int i = 0; i < DISPLAY_HEIGHT; i++) {
		for (int j = 0; j < char_num; j++) {
			if (isEven) {
				c1 = ((j < offset) || (j >= offset + length)) ? ' ' : text[j - offset];
				c_arr1 = font.at(c1);
				buffer.push_back(c_arr1[i]);
			} else {
				c1 = ((j <= offset) || (j > offset + length)) ? ' ' : text[j - offset - 1];
				c2 = ((j < offset) || (j >= offset + length)) ? ' ' : text[j - offset];
				c_arr1 = font.at(c1);
				c_arr2 = font.at(c2);
				buffer.push_back((c_arr1[i] & 0x0f) << 4 | (c_arr2[i] & 0xf0) >> 4);
			}
		}
	}
}
