#ifndef CMDS_HPP
#define CMDS_HPP

#include "BinLedCommand.hpp"
#include "TxtLedCommand.hpp"


class FileStartLedCommand : public TxtLedCommand {
public:
	FileStartLedCommand() : TxtLedCommand({ 0x00, 0x24, 0x00, 0x46 }, 5, 0x02) {};
};

class FileDataLedCommand : public BinLedCommand {
public:
	FileDataLedCommand() : BinLedCommand({ 0x00, 0x00, 0x00, 0x00 }, 5, 0x03) {};
};

class FileEndLedCommand : public TxtLedCommand {
public:
	FileEndLedCommand() : TxtLedCommand({ 0x00, 0x22, 0x00, 0x46 }, 3, 0x04) {};
};

class SetLuminanceLedCommand : public TxtLedCommand {
public:
	SetLuminanceLedCommand() : TxtLedCommand({ 0x00, 0x2e, 0x00, 0x46 }, 15, 0x15) {};
};

class RebootLedCommand : public TxtLedCommand {
public:
	RebootLedCommand() : TxtLedCommand({ 0x00, 0x21, 0x00, 0x46 }, 2, 0x16) {};
};

class ChangeProgramIndexLedCommand : public TxtLedCommand {
public:
	ChangeProgramIndexLedCommand() : TxtLedCommand({ 0x00, 0x26, 0x00, 0x46 }, 7, 0x17) {};
};

class SetPlayModeLedCommand : public TxtLedCommand {
public:
	SetPlayModeLedCommand() : TxtLedCommand({ 0x00, 0x26, 0x00, 0x46 }, 7, 0x26) {};
};

#endif
