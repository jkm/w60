#include <errno.h>
#include <string.h>
#include <vector>

#include "Socket.hpp"


Socket::Socket() : sock(-1) {
	memset(&addr, 0, sizeof(addr));
}

Socket::~Socket() {
	::close(sock);
}

bool Socket::create() {
	sock = socket(AF_INET, SOCK_STREAM, 0);

	if (!isValid())
		return false;

	return true;
}

bool Socket::bind(int port) {
	if (!isValid())
		return false;

	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(port);

	int ret;

	ret = ::bind(sock, (struct sockaddr *)&addr, sizeof(addr));
	if (ret < 0)
		return false;

	return true;
}

bool Socket::listen() const {
	if (!isValid())
		return false;

	int ret;

	ret = ::listen(sock, SOCKET_MAX_CONNECTIONS);
	if (ret == -1)
		return false;

	return true;
}

bool Socket::accept(Socket & socket) const {
	int length = sizeof(addr);
	socket.sock = ::accept(sock, (sockaddr *)&addr, (socklen_t *)&length);

	if (socket.sock <= 0)
		return false;

	return true;
}

bool Socket::send(const std::vector<uint8_t> data) const {
	int ret;

	ret = ::send(sock, (const char *)data.data(), data.size(), MSG_NOSIGNAL);
	if (ret == -1)
		return false;

	return true;
}

int Socket::recv(std::string & s) const {
	char buf[SOCKET_MAX_RECV + 1];

	s = "";

	memset(buf, 0, SOCKET_MAX_RECV + 1);

	int ret;
	ret = ::recv(sock, buf, SOCKET_MAX_RECV, 0);

	if (ret <= 0)
		return 0;

	s = buf;

	return ret;
}

bool Socket::connect(const std::string host, const int port) {
	if (!isValid())
		return false;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);

	int ret;
	ret = inet_pton(AF_INET, host.c_str(), &addr.sin_addr);

	if (errno == EAFNOSUPPORT)
		return false;

	ret = ::connect(sock, (sockaddr *)&addr, sizeof(addr));

	if (ret != 0)
		return false;

	return true;
}

bool Socket::isValid() const {
	return sock != -1;
}
