#include <cstdlib>

#include "TxtLedCommand.hpp"


std::vector<uint8_t> TxtLedCommand::toBuffer(std::vector<uint8_t> arg, uint32_t randNum, char *timeStr) {
	std::vector<uint8_t> buffer;
	int len = length + 104;

	buffer.reserve(len);

	buffer.insert(buffer.end(), header.begin(), header.end());

	buffer.push_back(CMD_TXT_HEADER);
	buffer.push_back((len >> 8) & 0xff);
	buffer.push_back(len & 0xff);
	buffer.push_back(nonce);

	buffer.insert(buffer.end(), string.begin(), string.end());

	for (int i = 0; i < sizeof(randNum); i++)
		buffer.push_back((randNum >> i) & 0xff);

	buffer.insert(buffer.end(), code.begin(), code.end());
	buffer.insert(buffer.end(), arg.begin(), arg.end());
	buffer.insert(buffer.end(), footer.begin(), footer.end());

	buffer.push_back(0x2c);
	buffer.insert(buffer.end(), sep.begin(), sep.end());
	buffer.push_back(0x2c);

	for (int i = 0; i < 19; i++)
		buffer.push_back(uint8_t(timeStr[i]));

	buffer.insert(buffer.end(), sep.begin(), sep.end());

	free(timeStr);

	return buffer;
}
