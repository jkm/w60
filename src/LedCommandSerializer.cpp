#include <chrono>
#include <ctime>
#include <iomanip>
#include <iostream>

#include "LedCommand.hpp"
#include "LedCommandSerializer.hpp"


using std::chrono::system_clock;

void LedCommandSerializer::clearArg() {
	argBuf.clear();
}

void LedCommandSerializer::clearBuffer() {
	buffer.clear();
}

void LedCommandSerializer::insertArg(std::vector<uint8_t> buf) {
	argBuf.insert(argBuf.end(), buf.begin(), buf.end());
}

char *LedCommandSerializer::getDateTime() {
	char *timeStr = (char *)malloc(sizeof(char) * 20);

	system_clock::time_point now = system_clock::now();
	std::time_t dt = system_clock::to_time_t(now);
	std::strftime(timeStr, 20, "%G/%m/%d %H:%M:%S", std::localtime(&dt));

	return timeStr;
}

uint32_t LedCommandSerializer::getRandNum() {
	return rand() % 0xffffffff;
}

void LedCommandSerializer::signBuffer() {
	int s = 0;
	for (int i = 0; i < buffer.size(); i++)
		s += buffer[i] & 0xff;

	buffer.push_back((s >> 8) & 0xff);
	buffer.push_back(s & 0xff);
	buffer.push_back(0xaa);
}

void LedCommandSerializer::printBuffer() {
	for (uint8_t i : buffer)
		std::cout << std::setfill('0') << std::setw(2) << std::hex << int(i) << " ";

	std::cout << std::endl;
	std::cout << std::dec << buffer.size() << std::endl;
}

void LedCommandSerializer::setBuffer() {
	clearBuffer();
	insertBuffer(cmd->toBuffer(argBuf, getRandNum(), getDateTime()));
	signBuffer();
}

std::vector<uint8_t> LedCommandSerializer::getBuffer() {
	return buffer;
}

std::vector<uint8_t> LedCommandSerializer::serialize(std::vector<uint8_t> arg) {
	clearArg();
	insertArg(arg);

	setBuffer();

	return buffer;
}

std::vector<uint8_t> LedCommandSerializer::serialize(std::vector<uint8_t> arg, std::vector<BufferedEntity *> buffers) {
	clearArg();
	insertArg(arg);

	for (auto & b : buffers)
		insertArg(b->getBuffer());

	setBuffer();

	return buffer;
}
