#ifndef BIN_LED_COMMAND_HPP
#define BIN_LED_COMMAND_HPP

#include "LedCommand.hpp"


class BinLedCommand : public LedCommand {
public:
	BinLedCommand(code_t code, uint8_t length, uint8_t nonce) : LedCommand(code, length, nonce) {};

	std::vector<uint8_t> toBuffer(std::vector<uint8_t> arg, uint32_t randNum, char *timeStr);
};

#endif
