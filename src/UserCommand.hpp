#ifndef USER_COMMAND_HPP
#define USER_COMMAND_HPP

#include "cmds.hpp"
#include "LedSender.hpp"
#include "LedCommandSerializer.hpp"


class UserCommand : public LedSender {
public:
	UserCommand(LedCommandSerializer *serializer) : LedSender(), serializer(serializer) {}

	~UserCommand() {
		delete serializer;
	}

	void setSerializer(LedCommandSerializer *s);

protected:
	LedCommandSerializer *serializer;
};

class BinaryUserCommand : public UserCommand {
public:
	BinaryUserCommand(LedCommandSerializer *serializer) : UserCommand(serializer) {}

	void handle(std::vector<BufferedEntity *> buffers);
};

class RebootUserCommand : public UserCommand {
public:
	RebootUserCommand() : UserCommand(new LedCommandSerializer(new RebootLedCommand())) {}

	void handle();
};

class SetLuminanceUserCommand : public UserCommand {
public:
	SetLuminanceUserCommand() : UserCommand(new LedCommandSerializer(new SetLuminanceLedCommand())) {}

	void handle(uint8_t luminance);
};

class SetHdsUserCommand : public BinaryUserCommand {
public:
	SetHdsUserCommand() : BinaryUserCommand(nullptr) {}

	void handle(std::string path);
};

class SetTextUserCommand : public BinaryUserCommand {
public:
	SetTextUserCommand() : BinaryUserCommand(nullptr) {}

	void handle(std::string path, std::string text);
};

#endif
