#ifndef BUFFERED_ENTITTY_HPP
#define BUFFERED_ENTITTY_HPP

#include <cstdint>
#include <vector>


class BufferedEntity {
public:
	void insertBuffer(std::vector<uint8_t> buf);
	std::vector<uint8_t> getBuffer();

protected:
	std::vector<uint8_t> buffer;
};

#endif
