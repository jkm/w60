#include "BufferedEntity.hpp"


void BufferedEntity::insertBuffer(std::vector<uint8_t> buf) {
	buffer.insert(buffer.end(), buf.begin(), buf.end());
}

std::vector<uint8_t> BufferedEntity::getBuffer() {
	return buffer;
}
