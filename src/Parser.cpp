#include "Parser.hpp"


void SetHdsParserHandler::handle() {
	std::string path = get<std::string>("path");

	SetHdsUserCommand cmd;
	cmd.handle(path);
}

void SetTextParserHandler::handle() {
	std::string path = get<std::string>("path");
	std::string text = get<std::string>("text");

	SetTextUserCommand cmd;
	cmd.handle(path, text);
}

void SetLuminanceParserHandler::handle() {
	int luminance = get<int>("luminance");

	SetLuminanceUserCommand cmd;
	cmd.handle(luminance);
}

void RebootParserHandler::handle() {
	RebootUserCommand cmd;
	cmd.handle();
}

void ProgramParserHandler::handle() {
	for (auto & p : parsers)
		if (is_subcommand_used(*p))
			p->handle();
}
