#include <iomanip>
#include <iostream>
#include <vector>

#include "ByteArray.hpp"


template<std::size_t N>
ByteArray<N>::ByteArray() {
	buffer.fill(0x00);
	position = 0;
}

template<std::size_t N>
std::vector<uint8_t> ByteArray<N>::toVector() {
	std::vector<uint8_t> v_buffer;
	v_buffer.reserve(position);
	v_buffer.insert(v_buffer.begin(), buffer.begin(), buffer.begin() + position);
	return v_buffer;
}

// b
template<std::size_t N>
std::array<uint8_t, N> ByteArray<N>::getBuffer() {
	return buffer;
}

// e
template<std::size_t N>
std::size_t ByteArray<N>::getLength() {
	return N;
}

// f
template<std::size_t N>
int ByteArray<N>::getPosition() {
	return position;
}

// h
template<std::size_t N>
void ByteArray<N>::setTwoBytes(int value) {
	setTwoPositionBytes(position, value);
	position += 2;
}

// i
template<std::size_t N>
void ByteArray<N>::setTwoPositionBytes(int position, int value) {
	buffer[position] = (value >> 8) & 0xff;
	buffer[position + 1] = value & 0xff;
}

// j
template<std::size_t N>
void ByteArray<N>::setThreeBytes(int value) {
	setThreePositionBytes(position, value);
	position += 3;
}

// k
template<std::size_t N>
void ByteArray<N>::setThreePositionBytes(int position, int value) {
	buffer[position] = (value >> 16) & 0xff;
	buffer[position + 1] = (value >> 8) & 0xff;
	buffer[position + 2] = value & 0xff;
}

// l
template<std::size_t N>
void ByteArray<N>::setFourBytes(int value) {
	setFourPositionBytes(position, value);
	position += 4;
}

// m
template<std::size_t N>
void ByteArray<N>::setFourPositionBytes(int position, int value) {
	buffer[position] = (value >> 24) & 0xff;
	buffer[position + 1] = (value >> 16) & 0xff;
	buffer[position + 2] = (value >> 8) & 0xff;
	buffer[position + 3] = value & 0xff;
}

// n
template<std::size_t N>
void ByteArray<N>::setOneByte(int value) {
	setOnePositionByte(position, value);
	position++;
}

// o
template<std::size_t N>
void ByteArray<N>::setOnePositionByte(int position, int value) {
	buffer[position] = value & 0xff;
}

// p
template<std::size_t N>
void ByteArray<N>::incrementPosition(int step) {
	position += step;
}

template<std::size_t N>
void ByteArray<N>::print() {
	for (int i = 0; i < position; i++)
		std::cout << std::setfill('0') << std::setw(2) << std::hex << int(buffer[i]) << " ";

	std::cout << std::endl;
}

template class ByteArray<256>;
