#ifndef LED_COMMAND_SERIALIZER_HPP
#define LED_COMMAND_SERIALIZER_HPP

#include <cstdint>
#include <vector>

#include "BitmapRenderer.hpp"
#include "JsonSerializer.hpp"
#include "HdsSerializer.hpp"


class LedCommandSerializer {
public:
	LedCommandSerializer(LedCommand *cmd) : cmd(cmd) {};
	~LedCommandSerializer() {
		delete cmd;
	}

	char *getDateTime();
	uint32_t getRandNum();

	void clearArg();
	void clearBuffer();

	void insertArg(std::vector<uint8_t> buf);

	template<typename Container>
	void insertBuffer(const Container & buf) {
		buffer.insert(buffer.end(), buf.begin(), buf.end());
	}

	std::vector<uint8_t> serialize(std::vector<uint8_t> arg);
	std::vector<uint8_t> serialize(std::vector<uint8_t> arg, std::vector<BufferedEntity *> buffers);

	void signBuffer();
	void printBuffer();
	void setBuffer();

	std::vector<uint8_t> getBuffer();
	std::vector<uint8_t> getHdsBuffer(std::string path);

private:
	std::vector<uint8_t> argBuf;
	std::vector<uint8_t> buffer;

	LedCommand *cmd;
};

#endif
