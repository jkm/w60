#include "Parser.hpp"


int main(int argc, char *argv[]) {
	srand(time(NULL));

	ProgramParserHandler program;

	try {
		program.parse_args(argc, argv);
	} catch (const std::runtime_error & err) {
		std::cerr << err.what() << std::endl;
		std::cerr << program;
		std::exit(1);
	}

	program.handle();

	return 0;
}
