# w60

A futile attempt to reverse-engineer the Huidu W60 LED controller protocol

## Building:
- Install necessary packages:
```
$ sudo apt install libjsoncpp-dev -y
```

- Then build the `w60` binary:
```
$ make
```

## Testing:
- Turn on your LED array, connect to it and run the binary:
```
$ ./w60 screen.json "HELLO WORLD!"
```

Keep your fingers crossed that it will work, but it will probably not...
