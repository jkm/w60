#include <catch2/catch_test_macros.hpp>

#include <algorithm>

#include "ByteArray.hpp"


TEST_CASE("ByteArray size", "[size]") {
	ByteArray<256> array;

	REQUIRE(array.getLength() == 256);
}

TEST_CASE("ByteArray empty", "[empty]") {
	ByteArray<256> array;

	SECTION("props") {
		REQUIRE(array.getPosition() == 0);
	}

	SECTION("buffer") {
		auto arr = array.getBuffer();

		REQUIRE(std::all_of(arr.begin(), arr.end(), [](uint8_t i) {
			return i == 0;
		}));
	}

	SECTION("vector") {
		std::vector<uint8_t> v = array.toVector();

		REQUIRE(v.size() == 0);

		REQUIRE(std::all_of(v.begin(), v.end(), [](uint8_t i) {
			return i == 0;
		}));
	}
}

TEST_CASE("ByteArray filled with data", "[data]") {
	ByteArray<256> array;

	array.setOneByte(0x48);
	array.setOneByte(0x50);
	array.setTwoBytes(0x00);
	array.setOneByte(0x01);

	SECTION("props") {
		auto arr = array.getBuffer();
		int position = array.getPosition();
		std::array<uint8_t, 5> exp = { 0x48, 0x50, 0x00, 0x00, 0x01 };

		REQUIRE(position == 5);

		REQUIRE(std::equal(arr.begin(), arr.begin() + position, exp.begin(), exp.end()));

		REQUIRE(std::all_of(arr.begin() + position, arr.end(), [](uint8_t i) {
			return i == 0;
		}));
	}
}
