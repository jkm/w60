CXX := g++
BIN := w60
TESTS := tests

BIN_DIR = bin
SRC_DIR = src
TEST_DIR = tests

I_FLAGS    = -I $(SRC_DIR)
LD_FLAGS   = -ljsoncpp
T_LD_FLAGS = -lCatch2Main -lCatch2
CXX_FLAGS  = -std=c++17

SRCS = $(SRC_DIR)/BinLedCommand.cpp \
       $(SRC_DIR)/TxtLedCommand.cpp \
       $(SRC_DIR)/BufferedEntity.cpp \
       $(SRC_DIR)/ByteArray.cpp \
       $(SRC_DIR)/LedSender.cpp \
       $(SRC_DIR)/LedCommand.cpp \
       $(SRC_DIR)/LedCommandSerializer.cpp \
       $(SRC_DIR)/UserCommand.cpp \
       $(SRC_DIR)/BitmapRenderer.cpp \
       $(SRC_DIR)/FileSerializer.cpp \
       $(SRC_DIR)/JsonSerializer.cpp \
       $(SRC_DIR)/HdsSerializer.cpp \
       $(SRC_DIR)/Parser.cpp \
       $(SRC_DIR)/Socket.cpp \
       $(SRC_DIR)/main.cpp

T_SRCS = $(TEST_DIR)/ByteArray.cpp

OBJS       = $(patsubst %.cpp,%.o,$(SRCS))
T_OBJS     = $(patsubst %.cpp,%.o,$(T_SRCS))
T_SRC_OBJS = $(patsubst $(TEST_DIR)%.o,$(SRC_DIR)%.o,$(T_OBJS))

all: $(BIN) $(TESTS)

$(BIN): $(OBJS)
	$(CXX) $(OBJS) -o $(BIN_DIR)/$@ $(LD_FLAGS)

$(TESTS): $(T_OBJS) $(T_SRC_OBJS)
	$(CXX) $(T_OBJS) $(T_SRC_OBJS) -o $(BIN_DIR)/$@ $(T_LD_FLAGS)

$(SRC_DIR)/%.o : ./$(SRC_DIR)/%.cpp
	$(CXX) -c $< -o $@ $(CXX_FLAGS) $(I_FLAGS)

$(TEST_DIR)/%.o : ./$(TEST_DIR)/%.cpp
	$(CXX) -c $< -o $@ $(CXX_FLAGS) $(I_FLAGS)

clean:
	-rm -f $(BIN_DIR)/$(BIN) $(BIN_DIR)/$(TESTS) $(OBJS) $(T_OBJS)
